<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
	<div class="row">

        <div class="col-lg-12 text-center">
          <h1 class="mt-5">Rock Paper Scissors Lizard Spock</h1>
          <p class="lead">Quick RPSLS simulated with the power of humans</p>
          <h2>Tech</h2>
          <ul class="list-unstyled">
            <li>Codeigniter 3.1.6</li>
            <li>jQuery 3.2.1</li>
            <li>Bootstrap 3.3.7</li>
            <li>jQuery 3.2.1</li>
            <li>HTML/CSS/JS/PHP/MYSQL</li>
            <li>OOP + MVC</li>
          </ul>
          <br><br><br>
        </div>
	</div>
  
  
	<div class="row">

        <div class="col-xs-4"></div>
        <div class="col-xs-4 text-center">			
			<?=validation_errors('<div class="alert alert-danger">','</div>')?>
			
			
			<?=form_open()?>
			
			<fieldset>
				<div class="form-group">
				  <label for="username" class="control-label big-label">Username</label>
				  
				    <input type="text" class="input-lg form-control" id="username" name="username" maxlength="12" required="" <?php echo set_value('username'); ?>> 
				    
				  
				</div>
				
				<div class="form-group">
				  <label class="control-label" for=""></label>
				  <div class="text-center">
				    <button type="submit" value="Submit" class="btn btn-success btn-lg" aria-label="">Submit</button>
				    
				  </div>
				</div>
			</fieldset>
			
			</form>
        </div>
        <div class="col-xs-4"></div>
	</div>

</div><!-- /.container -->