<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageMissing extends CI_Controller {

	/**
	 * 404 page for this controller.
	 */
	public function index()
	{
		$this->load->view('404');
	}
}
