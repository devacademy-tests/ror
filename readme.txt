###################
Live Version
###################

https://playmore.ro/ror/



###################
Git
###################

https://gitlab.com/devacademy-tests/ror



###################
Rquirements
###################

PHP 5.6+
MySQL 5.5+
Apache 2.2+



###################
Installation
###################

Import /ror.sql into a MySQL server
Configure the credentials in /application/config/database.php
Set the base url in /application/config/config.php